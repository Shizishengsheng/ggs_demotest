package com.example.demo.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Guan Jinghui
 * @Description:
 * @Date: Created in 3:50 下午 2020/12/25
 * @Modified By:
 **/
@RestController
@RequestMapping("/user")
public class HelloController {

    @RequestMapping("/hello")
    public String Hello(){
        return"hello world";
    }
}
